/* Модели и методы анализа проектных решений. Домашнее задание. */
/* Макаренко Н.А. РК6-74Б Метод 5 - Табличный. Схема 08.        */

/* Исходный файл класса вектора */

#include "vector.h"

Vector::Vector(): _size(SIZE)
{
    _vector.reserve(_size);
    for (int i = 0; i < _size; ++i)
    {
        _vector.push_back(0.0);
    }
}

void Vector::calcB(Vector& Prev, Vector& Cur, double t, double dt)
{
    double Lc = L / dt;
    double C1c = C1 / dt;
    double C2c = C2 / dt;
    double Cbc = Cb / dt;

    _vector[0] = -(Cur[0] + Cur[12] - Cur[13]);
    _vector[1] = -(Cur[1] - Cur[15]);
    _vector[2] = -(Cur[2] - Cur[15]);
    _vector[3] = -(Cur[3] - Cur[13] + Cur[14] + Cur[15]);

    _vector[4] = -(Cur[4] - Cur[8]);
    _vector[5] = -(Cur[5] + Cur[8] + Cur[11]);
    _vector[6] = -(Cur[6] - Cur[11]);
    _vector[7] = -(Cur[7] + Cur[9] + Cur[10] - Cur[11]);

    _vector[8] = -(Cur[0] - Lc*(Cur[8] - Prev[8]));
    _vector[9] = -(Cur[9] - It*(exp(Cur[1]/MFt) - 1));
    _vector[10] = -(Cur[2] - Cur[10]*Ry);
    _vector[11] = -(Cur[3] - Cur[11]*Rb);

    _vector[12] = -(Cur[12] - A*sin(2 * M_PI / T * t));
    _vector[13] = -(Cur[5] - C1c*(Cur[13] - Prev[13]));
    _vector[14] = -(Cur[6] - C2c*(Cur[14] - Prev[14]));
    _vector[15] = -(Cur[7] - Cbc*(Cur[15] - Prev[15]));
}

Vector& Vector::operator =(const Vector& right)
{
    if (this == &right)
    {
        return *this;
    }

    for (int i = 0; i < _size; ++i)
    {
        _vector[i] = right._vector[i];
    }

    return *this;
}

Vector& operator +=(Vector& left, Vector& right)
{
    for (int i = 0; i < left._size; ++i)
    {
        left._vector[i] += right[i];
    }

    return left;
}

double& Vector::operator [](int i)
{
    return _vector[i];
}

void Vector::calcInitApprox(Vector& Prev, Vector& Cur, double dtPrev, double dtCur)
{
    for (int i = 0; i < _size; i++)
    {
        _vector[i] = (dtPrev - dtCur)/dtPrev * (Cur[i] - Prev[i]) + Prev[i];
    }
}

double Vector::vectorNorm() const
{
    double cur = 0.0;
    for (int i = 0; i < _size; ++i)
    {
        if (fabs(_vector[i]) > cur)
        {
            cur = fabs(_vector[i]);
        }
    }
    return cur;
}

std::ostream& operator <<(std::ostream& out, const Vector& X)
{
    int i = 0;
    for (const auto& elem : X._vector)
    {
        out << i << ": " << setw(4) << elem << endl;
        ++i;
    }
    out << "----------------------------------" << endl;
    return out;
}
