/* Модели и методы анализа проектных решений. Домашнее задание. */
/* Макаренко Н.А. РК6-74Б Метод 5 - Табличный. Схема 08.        */

/* Исходный файл с функциями расчета */

#include "const.h"
#include "matrix.h"
#include "vector.h"

double calcEps(Vector& Prev, Vector& Cur, Vector& New, double dtPrev, double dtCur)
{
    double max_eps = 0.0;
    double eps;
    for (int i = 0; i < SIZE; ++i)
    {
        eps = 0.5 * dtCur * dtCur * fabs((New[i] - Cur[i])/dtCur - (Cur[i] - Prev[i])/dtPrev);
        if (eps > max_eps)
        {
            max_eps = eps;
        }
    }
    return max_eps;
}

void plot()
{
    FILE *gnu = popen("gnuplot -persist", "w");
    fprintf (gnu, "set grid xtics ytics\n");
    fprintf (gnu, " set key top center\n");
    fprintf (gnu, "plot \"U.txt\" using 1:2 w li lw 2 lt rgb 'red' title 'UC1'\n");
}

// Функция решения СЛАУ Ax = b методом Гаусса с выбором главного элемента
Vector GaussSolve(Matrix Coef, Vector B)
{
    Vector X;
    // Прямой ход
    for (int k = 0; k < SIZE; ++k)
    {
        // Находим максимальный элемент в столбце k
        double maxEl = Coef[k][k];
        int indMax = k;
        for (int i = k + 1; i < SIZE; ++i)
        {
            if (fabs(maxEl) < fabs(Coef[i][k]))
            {
                maxEl = Coef[i][k];
                indMax = i;
            }
        }
        // Меняем местами
        if (k != indMax)
        {
            // Меняем строку с максимальным элементом местами с первой строкой
            vector<double> tmpLine = Coef[k];
            Coef[k] = Coef[indMax];
            Coef[indMax] = tmpLine;
            // Меняем соответствующие элементы в векторе b
            double tmpEl = B[k];
            B[k] = B[indMax];
            B[indMax] = tmpEl;
        }
        if (fabs(Coef[k][k]) < EPS_GAUSS)
        {
            printf("Решение получить невозможно из-за нулевого столбца ");
            printf("%d матрицы A\n", k);
            exit(-2);
        }
        // Вычитаем из строки i строку k умноженную на коэффициент coeff
        for (int i = k + 1; i < SIZE; ++i)
        {
            double coeff = Coef[i][k] / Coef[k][k];
            for (int j = k; j < SIZE; ++j) {
                Coef[i][j] -= Coef[k][j] * coeff;
            }
            B[i] -= B[k] * coeff;
        }
    }

    // Делим каждую строку матрицы на соответствующий диагональный элемент
    for (int i = 0; i < SIZE; ++i)
    {
        double coeff = Coef[i][i];
        for (int j = i; j < SIZE; ++j)
        {
            if (fabs(Coef[i][j]) < EPS_GAUSS) continue;
            Coef[i][j] /= coeff;
        }
        B[i] /= coeff;
    }

    // Обратный ход
    for (int i = SIZE - 1; i >= 0; --i)
    {
        X[i] = Coef[i][i] * B[i];
        for (int j = i + 1; j < SIZE; ++j)
        {
            X[i] -= Coef[i][j] * X[j];
        }
    }
    return X;
}