/* Модели и методы анализа проектных решений. Домашнее задание. */
/* Макаренко Н.А. РК6-74Б Метод 5 - Табличный. Схема 08.        */

/* Основная управляющая функция, реализующая моделирование */

#include "matrix.h"
#include "vector.h"
#include "const.h"
#include <fstream>

// Функция решения СЛАУ вида Ax=b методом Гаусса с выбором главного элемента
Vector GaussSolve(Matrix, Vector);

// Функция вывода графика фазовой переменной посредством Gnuplot
void plot();

// Функция расчета локальной погрешности вычисления вектора фазовых переменных
double calcEps(Vector&, Vector&, Vector&, double, double);

using std::flush, std::cout, std::cerr;

int main()
{
    // Файл с данными для вывода графика фазовой переменной
    std::ofstream out_res;
    out_res.open("U.txt");

    // Файл для вывода матриц Якоби на каждой итерации метода Ньютона
    std::fstream out_matrix;
    out_matrix.open("matrix.txt", std::ios::out | std::ios::app);

    // Файл для вывода вектора невязок на каждой итерации метода Ньютона
    std::fstream out_vector;
    out_vector.open("vector.txt", std::ios::out | std::ios::app);

    // Файл для вывода вектора дельт на каждой итерации метода Ньютона
    std::fstream out_delta;
    out_delta.open("delta.txt", std::ios::out | std::ios::app);

    // Файл для вывода вектора фазовых переменных на каждой итерации метода Ньютона
    std::fstream out_phase;
    out_phase.open("phase.txt", std::ios::out | std::ios::app);


    Matrix Coef;    // Матрица Якоби (A)
    Vector B;       // Вектор свободных членов (b)
    Vector Delta;   // Вектор дельт (x)

    Vector Prev;    // Вектор фазовых переменных с предыдущего ШАГА
    Vector Cur;     // Вектор фазовых переменных на текущем ШАГЕ

    Vector CurApprox;   // Вектор начального приближения текущей ИТЕРАЦИИ

    double eps;         // Величина остаточного члена

    double t = 0.0;     // Текущее модельное время
    double dt = DT;     // Шаг по времени на текущем шаге
    double dtPrev = DT; // Шаг по времени на предыдущем шаге

    int iter_count;     // Счетчик Итераций на текущем шаге метода Ньютона
    int iter_sum = 0;   // Глобальный счетчик ИТЕРАЦИЙ метода Ньютона
    int step_count = 0; // Счетчик ШАГОВ метода Ньютона

    // Цикл моделирования схемы по ШАГАМ метода Ньютона
    while (t < TIME)
    {
        ++step_count;
        iter_count = 1;
        out_matrix << "Шаг #" << step_count << ":" << endl;
        out_vector << "Шаг #" << step_count << ":" << endl;
        out_delta << "Шаг #" << step_count << ":" << endl;
        out_phase << "Шаг #" << step_count << ":" << endl;
        // Цикл по ИТЕРАЦИЯМ в рамках одного шага метода Ньютона
        do
        {
            ++iter_sum;
            out_matrix << "Итерация #" << iter_count << ":" << endl;
            out_vector << "Итерация #" << iter_count << ":" << endl;
            out_delta << "Итерация #" << iter_count << ":" << endl;
            out_phase << "Итерация #" << iter_count << ":" << endl;

            // Расчет матрицы коэффициентов
            Coef.calcA(CurApprox, dt);

            // Расчет вектора свободных членов
            B.calcB(Cur, CurApprox, t, dt);

            // Решение СЛАУ -> вычисление вектора дельт
            Delta = GaussSolve(Coef, B);

            // Сдвиг начального приближения на рассчитанную дельту
            CurApprox += Delta;

            out_matrix << Coef << flush;
            out_vector << B << flush;
            out_delta << Delta << flush;
            out_phase << CurApprox << flush;

            // Проверка, соответствует ли вектор дельт заданной точности
            if (Delta.vectorNorm() <= ACR) // соответствует => заканчиваем итерации
            {
                break;
            }
            else // не соответствует
            {
                if (iter_count < ITER_MAX) // счетчик итераций не превысил допустимый => делаем след итерацию
                {
                    ++iter_count;
                }
                else // счетчик превышен => дробим шаг
                {
                    dt /= 2.0;
                    out_matrix << "Дробление шага (после ИТЕРАЦИИ)" << endl;
                    out_vector << "Дробление шага (после ИТЕРАЦИИ)" << endl;
                    out_delta << "Дробление шага (после ИТЕРАЦИИ)" << endl;
                    out_phase << "Дробление шага (после ИТЕРАЦИИ)" << endl;
                    if (dt <= DT_MIN) // шаг по времени перешел нижнюю границу
                    {
                        cerr << "Шаг по времени перешел нижнюю границу (после ИТЕРАЦИИ)" << endl;
                        exit(-1);
                    }
                    else // шаг по времени допустимый => начинаем итерации заново с новым шагом
                    {
                        iter_count = 1;
                        CurApprox.calcInitApprox(Prev, Cur, dtPrev, dt);
                    }
                }
            }
        }
        while (true);  // Цикл по итерациям

        // Расчет и учет погрешности
        eps = calcEps(Prev, Cur, CurApprox, dtPrev, dt);
        if (eps > EPS_MAX) // Погрешность выше допустимой => дробим шаг по времени
        {
            dt /= 2;
            out_matrix << "Дробление шага (после ШАГА)" << endl;
            out_vector << "Дробление шага (после ШАГА)" << endl;
            out_delta << "Дробление шага (после ШАГА)" << endl;
            out_phase << "Дробление шага (после ШАГА)" << endl;
            if (dt <= DT_MIN) // шаг по времени перешел нижнюю границу
            {
                cerr << "Шаг по времени перешел нижнюю границу (после ШАГА)" << endl;
                exit(-2);
            }
            else // шаг по времени допустимый => начинаем шаг заново с новым шагом по времени
            {
                CurApprox.calcInitApprox(Prev, Cur, dtPrev, dt);
            }
        }
        else if (eps < EPS_MIN) // Погрешность ниже требуемой => увеличиваем шаг по времени
        {
            dtPrev = dt;
            dt *= 2;
            out_matrix << "Удвоение шага (после ШАГА)" << endl;
            out_vector << "Удвоение шага (после ШАГА)" << endl;
            out_delta << "Удвоение шага (после ШАГА)" << endl;
            out_phase << "Удвоение шага (после ШАГА)" << endl;
            if (dt >= DT_MAX) // шаг по времени перешел верхнюю границу
            {
                dt = DT_MAX;
            }
            Prev = Cur;
            Cur = CurApprox;
            CurApprox.calcInitApprox(Prev, Cur, dtPrev, dt);

            out_res << t << " " << Cur[14] << endl;

            t += dt;
    	}
        else // Погрешность в пределах ожидаемой => делаем следующий шаг со старым шагом по времени
        {
            dtPrev = dt;
            Prev = Cur;
            Cur = CurApprox;
            CurApprox.calcInitApprox(Prev, Cur, dtPrev, dt);

            out_res << t << " " << Cur[14] << endl;

            t += dt;
        }
    }   // Цикл по шагам

    cout << "Общее число шагов: " << step_count << endl;
    cout << "Общее число итераций: " << iter_sum << endl;

    plot();

    return 0;
}
