/* Модели и методы анализа проектных решений. Домашнее задание. */
/* Макаренко Н.А. РК6-74Б Метод 5 - Табличный. Схема 08.        */

/* Заголовочный файл класса матрицы коэффициентов */

#ifndef COEF_MATRIX_H
#define COEF_MATRIX_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include "const.h"
#include "vector.h"

using std::vector;
using std::endl, std::setw, std::ostream;

class Matrix
{
private:
    // Матрица коэффициентов
    vector<vector<double>> _matrix;

    // Размерность матрицы
    const int _size;

public:
    // Конструктор инициализации матрицы
    Matrix();

    // Расчет матрицы коэффициентов с учетом текущего вектора фазовых переменных и шага по времени
    void calcA(Vector&, double);

    // Перегрузка оператора индексирования
    vector<double>& operator [](int);

    // Оператор печати матрицы в файл
    friend ostream& operator <<(ostream&, const Matrix&);
};

#endif //COEF_MATRIX_H
