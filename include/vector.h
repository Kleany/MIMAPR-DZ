/* Модели и методы анализа проектных решений. Домашнее задание. */
/* Макаренко Н.А. РК6-74Б Метод 5 - Табличный. Схема 08.        */

/* Заголовочный файл класса вектора */

#ifndef FREE_VECTOR_H
#define FREE_VECTOR_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include "const.h"

using std::vector;
using std::endl, std::setw, std::ostream;

class Vector
{
private:
    // Вектор-столбец свободных членов
    vector<double> _vector;

    // Размерность вектора
    const int _size;

public:
    // Инициализация столбца нулями
    Vector();

    // Расчет столбца свободных членов
    void calcB(Vector&, Vector&, double, double);

    // Функция расчета вектора начального приближения (линейная экстраполяция)
    void calcInitApprox(Vector&, Vector&, double, double);

    // Функция, возвращающая супремум-норму вектора
    [[nodiscard]] double vectorNorm() const;

    // Перегрузка операторов
    double& operator [](int);
    Vector& operator =(const Vector&);
    friend Vector& operator +=(Vector&, Vector&);

    // Оператор печати вектора в файл
    friend ostream& operator <<(ostream&, const Vector&);
};

#endif //FREE_VECTOR_H
